test("test1", function() {
  var a = 2;
  var b = 10;
  var result = eval_prime(a,b);
  equal(result, 4);
});

test("test2", function() {
  var a = 4;
  var b = 29;
  var result = eval_prime(a,b);
  equal(result, 7);
});

test("test3", function() {
  var a = 2;
  var b = 1000000;
  var result = eval_prime(a,b);
  equal(result, 78498);
});

test("test4", function() {
  var a = 452841;
  var b = 906452;
  var result = eval_prime(a,b);
  equal(result, 33812);
});

